import { Component, OnInit } from '@angular/core';
import { Platform, LoadingController, NavController } from '@ionic/angular';
import { Router } from '@angular/router'
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapOptions,
  GoogleMapsEvent,
  LocationService,
  MyLocation,
  Marker,
  MarkerCluster,
  Environment
} from '@ionic-native/google-maps/ngx';

// import { GeolocationService } from './../../services/geolocation.service';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  private googleMapsApiKey: string = 'AIzaSyCcEwyVZlSciCI0UYi8m7RqMEFfOduBq30'; // 'AIzaSyCcEwyVZlSciCI0UYi8m7RqMEFfOduBq30';

  private map: GoogleMap;
  private loading: HTMLIonLoadingElement;
  public disablePage: boolean = true; // Wait to enable page content until page is fully loaded

  private posLat: number;
  private posLng: number;

  constructor(
    private platform: Platform,
    private loadingController: LoadingController,
    private navCtrl: NavController,
    // private geolocationService: GeolocationService
  ) { }

  async ngOnInit() {

    // Show spinner
    // this.loading = await this.loadingController.create({ spinner: 'dots', showBackdrop: false, message: 'Searching for your location...' });
    // await this.loading.present();

    // Check Location services and show Map
    await this.platform.ready();
    if (await LocationService.hasPermission()) {
      // await this.geolocationService.ready();
      await this.loadMap();
    } else {
      alert('Change your settings to allow this app to read your location');
    }

  }

  async loadMap() {

    // Google Maps API key for javascript API (iOS an Android in config.xml)
    Environment.setEnv({
      API_KEY_FOR_BROWSER_RELEASE: this.googleMapsApiKey,
      API_KEY_FOR_BROWSER_DEBUG: this.googleMapsApiKey
    });

    // Get current location
    const myLocation: MyLocation = await LocationService.getMyLocation();
    this.posLat = myLocation.latLng.lat;
    this.posLng = myLocation.latLng.lng;
    console.log('lat/lang=', this.posLat, this.posLng);

    // Get Map Options
    const options = this.getMapOptions();

    // Show map
    this.map = GoogleMaps.create('map_canvas', options);
    this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
      console.log('Map is ready');
      // this.loading.dismiss();
      this.disablePage = false;

      // Animation effect
      this.map.animateCamera({
        target: { lat: this.posLat, lng: this.posLng },
        zoom: 16,
        tilt: 0,
        bearing: 0,
        duration: 2500
      });

      // Set markers
      /*
      const marker: Marker = this.map.addMarkerSync({
        title: 'You',
        icon: 'blue',
        animation: 'DROP',
        position: {
          lat: this.posLat,
          lng: this.posLng
        }
      });
      marker.showInfoWindow();
      */

      const markerCluster: MarkerCluster = this.map.addMarkerClusterSync({
        markers: this.getOtherUserLocations(),
        icons: [
          /*
          {
            min: 1,
            max: 3,
            url: "./assets/markercluster/small.png",
            label: {
              color: "white"
            }
          },
          {
            min: 4,
            url: "./assets/markercluster/large.png",
            label: {
              color: "white"
            }
          }
          */
        ]
      });

    });

  }


  // Map Options
  getMapOptions(): GoogleMapOptions {

    const options: GoogleMapOptions = {

      camera: {
        target: {
          lat: this.posLat,
          lng: this.posLng
        },
        zoom: 12,
        bearing: 100,
      },

      controls: {
        compass: false,
        myLocationButton: true,
        myLocation: true,     // (blue dot)
        indoorPicker: false,
        zoom: false,          // android only
        mapToolbar: false     // android only
      },

      gestures: {
        scroll: true,
        tilt: false,
        zoom: true,
        rotate: false
      },

      styles: [

        { elementType: 'geometry', stylers: [{ color: '#dfe4da' }] },
        { elementType: 'labels.text.stroke', stylers: [{ color: '#dfe4da' }] },
        { elementType: 'labels.text.fill', stylers: [{ color: '#ffffff' }] },
        {
          featureType: 'administrative.locality',
          elementType: 'labels.text.fill',
          stylers: [{ color: '#393939' }]
        },
        {
          featureType: "poi",
          elementType: "labels",
          stylers: [
            { visibility: "off" }
          ]
        },
        {
          featureType: 'poi',
          elementType: 'labels.text.fill',
          stylers: [{ color: '#393939' }]
        },
        {
          featureType: 'poi.park',
          elementType: 'geometry',
          stylers: [{ color: '#c6e378' }]
        },
        {
          featureType: 'poi.park',
          elementType: 'labels.text.fill',
          stylers: [{ color: '#6b9a76' }]
        },
        {
          featureType: 'road',
          elementType: 'geometry',
          stylers: [{ color: '#ffffff' }]
        },
        {
          featureType: 'road',
          elementType: 'geometry.stroke',
          stylers: [{ color: '#ffffff' }]
        },
        {
          featureType: 'road',
          elementType: 'labels.text.fill',
          stylers: [{ color: '#4f4f4f' }]
        },
        {
          featureType: 'road.highway',
          elementType: 'geometry',
          stylers: [{ color: '#ffffff' }]
        },
        {
          featureType: 'road.highway',
          elementType: 'geometry.stroke',
          stylers: [{ color: '#ffffff' }]
        },
        {
          featureType: 'road.highway',
          elementType: 'labels.text.fill',
          stylers: [{ color: '#000000' }]
        },
        {
          featureType: 'transit',
          stylers: [
            { visibility: 'off' }
          ]
        },
        /*
        {
          featureType: 'transit',
          elementType: 'geometry',
          stylers: [{color: '#a3a79f'}]
        },
        {
          featureType: 'transit.station',
          elementType: 'labels.text.fill',
          stylers: [{color: '#393939'}]
        },
        */
        {
          featureType: 'water',
          elementType: 'geometry',
          stylers: [{ color: '#00bfc9' }]
        },
        {
          featureType: 'water',
          elementType: 'labels.text.fill',
          stylers: [{ color: '#515c6d' }]
        },
        {
          featureType: 'water',
          elementType: 'labels.text.stroke',
          stylers: [{ color: '#f1f4ee' }]
        }

      ], // https://developers.google.com/maps/documentation/javascript/style-reference

      // styles: [],

      preferences: {
        zoom: {
          minZoom: 12,
          maxZoom: 18
        },
      }

    };

    return (options);

  }


  // Get Other User Locations
  getOtherUserLocations() {

    // Demo data
    const dist = 0.002;
    return [
      {
        position: {
          lat: this.posLat + dist,
          lng: this.posLng + dist,
        },
        title: 'Nina test',
        //icon: 'assets/markercluster/marker.png'
      },
      {
        position: {
          lat: this.posLat - dist,
          lng: this.posLng - dist,
        },
        title: 'Olle test',
        //icon: 'assets/markercluster/marker.png'
      },
      {
        position: {
          lat: this.posLat + dist,
          lng: this.posLng - dist,
        },
        title: 'Karin test',
        //icon: 'assets/markercluster/marker.png'
      },
      {
        position: {
          lat: this.posLat - dist,
          lng: this.posLng + dist,
        },
        title: 'Sven test',
        //icon: 'assets/markercluster/marker.png'
      }
    ];

  }

}
